package com.github.mryf323.tractatus.experimental.extensions;


import com.github.mryf323.tractatus.ClauseDefinition;
import com.github.mryf323.tractatus.UniqueTruePoint;
import com.github.mryf323.tractatus.Valuation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(ReportingExtension.class)
@ClauseDefinition(clause = 'a', def = "a == b")
@ClauseDefinition(clause = 'b', def = "b == c")
@ClauseDefinition(clause = 'c', def = "c < d")
@ClauseDefinition(clause = 'd', def = "b < d")
public class SampleTest {

    private boolean predicateF(int x, int y, int z, int w) {
        if(x == y && (y == z || z < w && y != 0)) {
            return true;
        } else {
            return false;
        }
    }

    @UniqueTruePoint(
            cnf = "ab + acd",
    implicant = "ab",
    predicate = "a(b+cd)",
    valuations = {
                @Valuation(clause = 'a', valuation = true),
                @Valuation(clause = 'b', valuation = true),
                @Valuation(clause = 'c', valuation = false),
                @Valuation(clause = 'd', valuation = true)
            })
    @Test
    public void sampleUniqueTruePoint() {
        boolean value = predicateF(1,1, 1, -1);
        assertTrue(value);
    }
}
